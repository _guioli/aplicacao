class CreateCurriculums < ActiveRecord::Migration[5.2]
  def change
    create_table :curriculums do |t|
      t.text :description
      t.references :category, foreign_key: true
      t.references :member, foreign_key: true
      t.string :degree
      t.string :title, limit:250

      t.timestamps
    end
  end
end
